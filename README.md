# RTK Rails

**This gem still is under development.**

## React

### React Install

RTK-Rails provides the generator `rtk:react:install`. Its function is to set
everything up for React development. After adding the gem, just run:

```bash
rails g rtk:react:install
```

The generator `rtk:react:install` does the following:

1. **Installs Webpacker:** it runs `gem 'webpacker'` to add the Webpacker gem,
   `rake webpacker:install` to set up Webpacker and it removes the file
   `app/javascript/packs/application.js`, as it's automatically created by the
   Webpacker installer, but not used in this set up.

2. **Installs React:** it runs `rake webpacker:install:react` to set up
   Webpacker for React, it creates the file `app/javascript/packs/index.js`,
   which is the React entry point, and removes the file
   `app/javascript/packs/hello_react.jsx`, as it's automatically created by the
   Webpacker React installer, but not used in this set up. 

   **Installs React Router:** it runs `yarn add react-router-dom@6`, inserts
   `import { BrowserRouter } from "react-router-dom";` into `index.js`, wraps
   the `<App />` component inside the `BrowserRouter` component, insert `import
   { Routes, Route } from "react-router-dom";` into `App.js` and add a root
   route.

3. **Creates the Controller, View and Routes:** it creates the controller
   `StaticPages`, whose sole function is to render the index page; it creates
   the view `index`, whose sole function is to be the entry point for React;
   and it add a few routes to redirect all the requests to the index page,
   except the ones under the `api` scope.

## Redux

### Redux Install

RTK-Rails provides the generator `rtk:redux:install`. Its function is to set
everything up to use Redux in the project. After adding the gem, just run:

```bash
rails g rtk:redux:install
```

The generator `rtk:redux:install` does the following: 

1. **Creates**


## Query

### Query API

RTK-Rails provides the generator `rtk:query:api`. Its function is to generate a
API service definition.

```bash
rails g rtk:query:api apiName --baseUrl http://localhost:3000/
```

The generator `rtk:query:api` does the following: 

1. **Creates the API Service File:** It creates the file
   `app/javascript/services/apiName.js`, where it uses the function `createApi`
   to define an API service.

2. **Sets Up the API Service in the Redux Store:** It imports the previous file
   into the file `app/javascript/app/store.js`, and injects the relevant reducer
   and middlewire into the Redux store definition.

### Query Endpoints

(...)


## Devise

### Devise Install

RTK-Rails provides the generator `rtk:devise:install`. Its function is to set
everything up to use Devise in the project. The generator assumes that you have
RTK installed in your project. If you don't, then run:

```bash
bin/rails g rtk:install
```

If you already have RTK installed in your project, just run:

```
bin/rails g rtk:devise:install User 
```

The generator `rtk:devise:install` does the following: 

1. **Installs Devise:** it runs `gem 'devise'` to add the Devise gem,
   `rake g devise:install` to set up Devise.

2. **Configures Action Mailer:** it adds the following line to
   `config/environments/development.rb`:

```ruby
config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
```

3. **Generate Devise Models, Migrations and Routes:** it runs `bin/rails g
   devise User --skip-routes` to generate the model, the migration, and adds
   the following to the file `config/routes.rb`:

```ruby
  devise_scope :user do
    scope :users, defaults: { format: :json } do
      get '/current_user', to: 'users/sessions#show', as: ''

      # Sessions Routes
      get '/sign_in', to: 'static_pages#index', as: 'new_user_session', defaults: { format: :html }
      post '/sign_in', to: 'devise/sessions#create', as: 'user_session'
      delete '/sign_out', to: 'devise/sessions#destroy', as: 'destroy_user_session'

      # Passwords Routes
      get '/password/new', to: 'static_pages#index', as: 'new_user_password', defaults: { format: :html }
      get '/password/edit', to: 'static_pages#index', as: 'edit_user_password', defaults: { format: :html }
      patch '/password', to: 'devise/passwords#update', as: 'user_password'
      put '/password', to: 'devise/passwords#update', as: ''
      post '/password', to: 'devise/passwords#create', as: ''

      # Registrations Routes
      get '/cancel', to: 'devise/registrations#cancel', as: 'cancel_user_registration'
      get '/sign_up', to: 'static_pages#index', as: 'new_user_registration', defaults: { format: :html }
      get '/edit', to: 'static_pages#index', as: 'edit_user_registration', defaults: { format: :html }
      patch '', to: 'devise/registrations#update', as: 'user_registration'
      put '', to: 'devise/registrations#update', as: ''
      delete '', to: 'devise/registrations#destroy', as: ''
      post '', to: 'devise/registrations#create', as: ''
    end
  end
```

4. **Re-Enables Rails Cookies Middlewares:** it adds the following to the file
   `contig/application.rb`, which are necessary to re-enable Cookies and
   Sessions in the API-only mode, so Devise's session authentication can work:

```ruby
    config.session_store :cookie_store, key: '_interslice_session'
    config.middleware.use ActionDispatch::Cookies
    config.middleware.use ActionDispatch::Session::CookieStore
    config.middleware.use config.session_store, config.session_options
```

5. **Re-Enables Rails Cookies and Helpers Modules**: it adds the following to
   the file `app/controllers/application_controller.rb`:

```ruby
  include ActionController::Cookies
  include ActionController::Helpers
  include ActionController::MimeResponds

  clear_respond_to
  respond_to :json
```

These make the controllers to include the cookies, makes the controller helpers
available, and makes the devise controller respond to Json requests with Json.

6. **Creates a Current User Endpoint:** it generates the file
   `app/controllers/users/sessions_controller.rb` with the following content:

```ruby
# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  def show
    render json: current_user
  end
end
```

This makes an endpoint for the front end to get the current user's data.

7. **Creates the Redux Service Endpoints:** it adds the `User` tag type, the
   endpoints definitions, and the exports to the file
   `app/javascript/services/users.js`. The file will look something like this:

```javascript
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const usersApi = createApi({
  reducerPath: "usersApi",
  baseQuery: fetchBaseQuery({ baseUrl: "http://localhost:3000/api/users" }),
  tagTypes: [
    'User'
  ],

  endpoints: (builder) => ({
    // GET /current_user
    sessionsShow: builder.query({
      query: () => ({
        url: `/current_user`,
      }),
      providesTags: [{ type: 'User' }]
    }),

    // POST /sign_in
    sessionsCreate: builder.mutation({
      query: (data) => ({
        url: `/sign_in`,
        method: 'POST',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // DELETE '/sign_out'
    sessionsDestroy: builder.mutation({
      query: (data) => ({
        url: `/sign_out`,
        method: 'DELETE',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // POST '/password'
    passwordsCreate: builder.mutation({
      query: (data) => ({
        url: `/password`,
        method: 'POST',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // PATCH '/password'
    passwordsUpdate: builder.mutation({
      query: (data) => ({
        url: `/password`,
        method: 'PATCH',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // GET '/cancel'
    registrationsCancel: builder.mutation({
      query: () => ({
        url: `/cancel`,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // POST '/'
    registrationsCreate: builder.mutation({
      query: (data) => ({
        url: ``,
        method: 'POST',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // PATCH '/'
    registrationsUpdate: builder.mutation({
      query: (data) => ({
        url: ``,
        method: 'PATCH',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // DELETE '/'
    registrationsDestroy: builder.mutation({
      query: (data) => ({
        url: ``,
        method: 'DELETE',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // GET '/confirmation'
    confirmationsShow: builder.mutation({
      query: (data) => ({
        url: `/confirmation?confirmation_token=${data.confirmation_token}`,
        method: 'GET',
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // POST '/confirmation'
    confirmationsCreate: builder.mutation({
      query: (data) => ({
        url: `/confirmation`,
        method: 'POST',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // GET '/unlock'
    unlockShow: builder.mutation({
      query: (data) => ({
        url: `/unlock?unlock_token==${data.unlock_token}`,
        method: 'GET',
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

    // POST '/unlock'
    unlockCreate: builder.mutation({
      query: (data) => ({
        url: `/unlock`,
        method: 'POST',
        body: data,
      }),
      invalidatesTags: [{ type: 'User' }]
    }),

  }),
});

export const {  
  useSessionsShowQuery,
  useSessionsCreateMutation,
  useSessionsDestroyMutation,
  usePasswordsCreateMutation,
  usePasswordsUpdateMutation,
  useRegistrationsCancelMutation,
  useRegistrationsCreateMutation,
  useRegistrationsUpdateMutation,
  useRegistrationsDeleteMutation,
  useConfirmationsShowMutation,
  useConfirmationsCreateMutation,
  useUnlockShowMutation,
  useUnlockCreateMutation,
} = usersApi;
```

React Stuff: Create components.
