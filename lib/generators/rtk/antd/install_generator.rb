# frozen_string_literal: true

require 'rtk'

module Rtk
  module Antd
    module Generators
      # Sets up Ant Design
      class InstallGenerator < Rails::Generators::Base
        include Rtk::Helpers

        source_root File.expand_path('templates', __dir__)

        def install_antd
          run 'yarn add antd'

          insert_template_into_file 'app/javascript/packs/index.js',
                                    'antd_import.js.erb',
                                    after: %r{import App from "../App";[[:blank:]]*\n}
        end
      end
    end
  end
end
