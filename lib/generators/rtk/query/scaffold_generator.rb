# frozen_string_literal: true

require 'rtk'

module Rtk
  module Query
    module Generators
      # Generates Endpoints and insert them into the API interfaces
      class ScaffoldGenerator < Rails::Generators::NamedBase
        include Rtk::Helpers

        source_root File.expand_path('templates/scaffold/', __dir__)

        class_option :apiName,
                     required: true,
                     desc: '',
                     banner: ''

        def create_endpoints
          api = options[:apiName]

          insert_into_file "app/javascript/services/#{api.camelize(:lower)}.js",
                           "\"#{name.camelize.singularize}\", ",
                           after: 'tagTypes: ['

          insert_template_into_file "app/javascript/services/#{api.camelize(:lower)}.js",
                                    'endpoints_builder.js.erb',
                                    after: 'endpoints: (builder) => ({'

          insert_template_into_file "app/javascript/services/#{api.camelize(:lower)}.js",
                                    'endpoints_exports.js.erb',
                                    after: 'export const {'
        end
      end
    end
  end
end
