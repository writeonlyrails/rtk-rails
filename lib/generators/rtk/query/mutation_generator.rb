# frozen_string_literal: true

require 'rtk'

module Rtk
  module Query
    module Generators
      # Generates a POST endpoint and insert it into the API interface
      class MutationGenerator < Rails::Generators::NamedBase
        include Rtk::Helpers

        source_root File.expand_path('templates', __dir__)

        class_option :api, required: true, desc: '', banner: ''

        class_option :url, required: true, desc: '', banner: ''
        class_option :method, required: true, desc: '', banner: ''
        class_option :tags, required: true, desc: '', banner: ''

        def create_endpoints
          insert_template_into_file "app/javascript/services/#{options[:api].camelize(:lower)}.js",
                                    'mutation_builder.js.erb',
                                    after: /endpoints: \(builder\) => \({[[:blank:]]*\n/

          insert_template_into_file "app/javascript/services/#{options[:api].camelize(:lower)}.js",
                                    'mutation_export.js.erb',
                                    after: /export const {[[:blank:]]*\n/
        end
      end
    end
  end
end
