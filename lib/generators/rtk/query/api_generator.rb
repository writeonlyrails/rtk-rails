# frozen_string_literal: true

require 'rtk'

module Rtk
  module Query
    module Generators
      # Generates a new API interface
      class ApiGenerator < Rails::Generators::NamedBase
        include Rtk::Helpers

        source_root File.expand_path('templates', __dir__)

        class_option :baseUrl,
                     required: true,
                     default: 'http://localhost:3000/api/',
                     desc: '',
                     banner: ''

        def create_api
          template 'api.js.erb', "app/javascript/services/#{name.camelize(:lower)}.js"

          insert_template_into_file 'app/javascript/app/store.js',
                                    'api_import.js.erb',
                                    after: %r{import \{ configureStore \} from "@reduxjs/toolkit";[[:blank:]]*\n}

          insert_template_into_file 'app/javascript/app/store.js',
                                    'api_reducer.js.erb',
                                    after: /reducer: {[[:blank:]]*\n/

          insert_template_into_file 'app/javascript/app/store.js',
                                    'api_middleware.js.erb',
                                    after: /getDefaultMiddleware\(\)[[:blank:]]*\n/
        end
      end
    end
  end
end
