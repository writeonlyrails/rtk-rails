# frozen_string_literal: true

module Rtk
  module Generators
    # Runs Rtk::React and Rtk::Redux installs
    class InstallGenerator < Rails::Generators::Base
      def install_react
        generate 'rtk:react:install'
      end

      def install_redux
        generate 'rtk:redux:install'
      end

      def install_antd
        generate 'rtk:antd:install'
      end
    end
  end
end
