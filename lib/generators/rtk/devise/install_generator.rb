# frozen_string_literal: true

require 'rtk'

module Rtk
  module Devise
    module Generators
      # Setup Devise
      class InstallGenerator < Rails::Generators::Base
        include Rtk::Helpers

        source_root File.expand_path('templates/install/', __dir__)

        def install_devise
          gem 'devise'
          generate 'devise:install'

          insert_template_into_file 'config/environments/development.rb',
                                    'development.rb.erb',
                                    after: /Rails.application.configure do[[:blank:]]*\n/

          generate 'devise User --skip-routes'

          insert_template_into_routes 'routes.rb.erb'
        end

        def enable_cookies
          insert_template_into_file 'config/application.rb',
                                    'application.rb.erb',
                                    after: /class Application < Rails::Application[[:blank:]]*\n/

          insert_template_into_file 'app/controllers/application_controller.rb',
                                    'application_controller.rb.erb',
                                    after: /class ApplicationController < ActionController::API[[:blank:]]*\n/
        end

        def define_sessions_controller
          copy_file 'sessions_controller.rb.erb', 'app/controllers/users/sessions_controller.rb'
        end

        def define_api_and_endpoints
          copy_file 'users.js.erb', 'app/javascript/services/users.js'
        end

        def setup_store
          insert_template_into_file 'app/javascript/app/store.js',
                                    'store_import.js.erb',
                                    after: %r{import \{ configureStore \} from "@reduxjs/toolkit";[[:blank:]]*\n}

          insert_template_into_file 'app/javascript/app/store.js',
                                    'store_reducer.js.erb',
                                    after: /reducer: {[[:blank:]]*\n/

          insert_template_into_file 'app/javascript/app/store.js',
                                    'store_middleware.js.erb',
                                    after: /getDefaultMiddleware\(\)[[:blank:]]*\n/
        end

        def create_react_components
          copy_file 'PasswordsCreate.js', 'app/javascript/components/users/PasswordsCreate.js'
          copy_file 'PasswordsUpdate.js', 'app/javascript/components/users/PasswordsUpdate.js'
          copy_file 'RegistrationsCreate.js', 'app/javascript/components/users/RegistrationsCreate.js'
          copy_file 'RegistrationsDestroy.js', 'app/javascript/components/users/RegistrationsDestroy.js'
          copy_file 'RegistrationsUpdate.js', 'app/javascript/components/users/RegistrationsUpdate.js'
          copy_file 'SessionsCreate.js', 'app/javascript/components/users/SessionsCreate.js'
          copy_file 'SessionsDestroy.js', 'app/javascript/components/users/SessionsDestroy.js'
          copy_file 'SessionsShow.js', 'app/javascript/components/users/SessionsShow.js'
        end
      end
    end
  end
end
