import React, { useEffect } from "react";
import { Form, Button, Spin, Typography, message } from "antd";
import { useForm } from "antd/lib/form/Form";
import { useRegistrationsDestroyMutation, useSessionsShowQuery } from "../../services/users";

const RegistrationsDestroy = () => {
  const { error: currentUserError, isLoading: currentUserIsLoading, isError: currentUserIsError, } = useSessionsShowQuery();
  const [ registrationsDestroy, { data, error, isLoading, isSuccess, isError } ] = useRegistrationsDestroyMutation(); 
  const [form] = useForm();

  const onFinish = () => {
    registrationsDestroy();
    form.resetFields();
  };

  useEffect(() => {
    if (currentUserIsError) {
      message.error(currentUserError.data.error);
    }
  }, [currentUserError]);

  useEffect(() => {
    if (isError) {
      message.error(error.data.error);

      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }

    if (isSuccess) {
      // Redirect
      // console.log(data);
    }
  }, [data, error]);

  return (
    <Spin spinning={isLoading || currentUserIsLoading}>
      <Form form={form} onFinish={onFinish} name="users_destroy">
        <Typography.Title>Destroy</Typography.Title>

        <Form.Item>
          <Button disabled={currentUserIsError} danger type="primary" htmlType="submit">
            Destroy
          </Button>
        </Form.Item>
      </Form>
    </Spin>
  );
};

export default RegistrationsDestroy;
