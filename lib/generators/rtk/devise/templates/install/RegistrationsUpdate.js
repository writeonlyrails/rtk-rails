import React, { useEffect } from "react";
import { Form, Input, Button, Spin, Typography, message } from "antd";
import { useForm } from "antd/lib/form/Form";
import { useSessionsShowQuery, useRegistrationsUpdateMutation, } from "../../services/users";

const RegistrationsUpdate = () => {
  const { data: currentUserData, error: currentUserError, isLoading: currentUserIsLoading, isSuccess: currentUserIsSuccess, isError: currentUserIsError, } = useSessionsShowQuery();
  const [ registrationsUpdate, { data, error, isLoading, isSuccess, isError }, ] = useRegistrationsUpdateMutation(); 
  const [form] = useForm();

  useEffect(() => {
    if (currentUserIsError) {
      message.error(currentUserError.data.error);
    }

    if (currentUserIsSuccess) {
      form.resetFields();
      // console.log(currentUserData);
    }
  }, [currentUserData, currentUserError]);

  const onFinish = (values) => {
    registrationsUpdate({ id: currentUserData.id, user: values });
    form.resetFields();
  };

  useEffect(() => {
    if (isError) {
      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }

    if (isSuccess) {
      // Redirect
      // console.log(data);
    }
  }, [data, error]);

  // const disabled = () => currentUserIsError ? 'disabled' : null;

  return (
    <Spin spinning={isLoading || currentUserIsLoading}>
      <Form form={form} initialValues={currentUserData} onFinish={onFinish} name="users_update">

          <Typography.Title>Update</Typography.Title>

          <Form.Item name="email">
            <Input disabled={currentUserIsError} type="email" placeholder="Email" />
          </Form.Item>

          <Form.Item name="password" extra="Leave blank if you don't want to change it.">
            <Input disabled={currentUserIsError} type="password" placeholder="Password" />
          </Form.Item>

          <Form.Item name="password_confirmation">
            <Input disabled={currentUserIsError} type="password" placeholder="Password Confirmarion" />
          </Form.Item>

          <Form.Item name="current_password" extra="We need your current password to confirm your changes.">
            <Input disabled={currentUserIsError} type="password" placeholder="Current Password" />
          </Form.Item>

          <Form.Item>
            <Button disabled={currentUserIsError} type="primary" htmlType="submit">
              Update
            </Button>
          </Form.Item>

      </Form>
    </Spin>
  );
};

export default RegistrationsUpdate;
