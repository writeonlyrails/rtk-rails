import React, {useEffect} from "react";
import { Form, Input, Checkbox, Button, Spin, Typography, message } from "antd";
import { useForm } from "antd/lib/form/Form";
import { useSessionsCreateMutation  } from "../../services/users";

const SessionsCreate = () => {
  const [sessionsCreate, { data, error, isLoading, isSuccess, isError }] = useSessionsCreateMutation();

  const [form] = useForm();

  const onFinish = (values) => {
    sessionsCreate({ user: values })
    form.resetFields();
  };

  useEffect( () => {
    if (isError) {
      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }

    if (isSuccess) {
      console.log(data);
    }
  }, [data, error] )

  return (
    <Spin spinning={isLoading}>
      <Form form={form} onFinish={onFinish} name="sessions_create">
        <Typography.Title>Log In</Typography.Title>

        <Form.Item name="email">
          <Input type="email" placeholder="Email" />
        </Form.Item>

        <Form.Item name="password">
          <Input type="password" placeholder="Password" />
        </Form.Item>

        <Form.Item name="remember_me" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Log In
          </Button>
        </Form.Item>

      </Form>
    </Spin>
  );
};

export default SessionsCreate;
