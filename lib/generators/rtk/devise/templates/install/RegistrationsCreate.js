import React, {useEffect} from "react";
import { Form, Input, Button, Spin, Typography, message } from "antd";
import { useForm } from "antd/lib/form/Form";
import { useRegistrationsCreateMutation  } from "../../services/users";

const RegistrationsCreate = () => {
  const [usersCreate, { data, error, isLoading, isSuccess, isError }] = useRegistrationsCreateMutation();

  const [form] = useForm();

  const onFinish = (values) => {
    usersCreate({ user: values })
    form.resetFields();
  };

  useEffect( () => {
    if (isError) {
      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }

    if (isSuccess) {
      console.log(data);
    }
  }, [data, error] )

  return (
    <Spin spinning={isLoading}>
      <Form form={form} onFinish={onFinish} name="users_create">
        <Typography.Title>Create</Typography.Title>

        <Form.Item name="email">
          <Input type="email" placeholder="Email" />
        </Form.Item>

        <Form.Item name="password">
          <Input type="password" placeholder="Password" />
        </Form.Item>

        <Form.Item name="password_confirmation">
          <Input type="password" placeholder="Password Confirmarion" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Create
          </Button>
        </Form.Item>

      </Form>
    </Spin>
  );
};

export default RegistrationsCreate;
