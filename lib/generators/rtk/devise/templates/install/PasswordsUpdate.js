// GET /resource/password/edit?reset_password_token=abcdef
// Render the #edit only if coming from a reset password email link

import React, { useEffect } from "react";
import { Form, Input, Button, Spin, Typography, message } from "antd";
import { useForm } from "antd/lib/form/Form";
import { usePasswordsUpdateMutation, } from "../../services/users";

const PasswordsUpdate = () => {
  const [ passwordsUpdate, { data, error, isLoading, isSuccess, isError }, ] = usePasswordsUpdateMutation(); 
  const [form] = useForm();

  const onFinish = (values) => {
    passwordsUpdate({ password: values });
    form.resetFields();
  };

  useEffect(() => {
    if (isError) {
      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }

    if (isSuccess) {
      // Redirect
      // console.log(data);
    }
  }, [data, error]);

  // get reset password token from url
  return (
    <Spin spinning={isLoading}>
      <Form form={form} onFinish={onFinish} name="password_update">

          <Typography.Title>Change your password</Typography.Title>

          <Form.Item hidden={true} name="reset_password_token">
            <Input type="password" />
          </Form.Item>

          <Form.Item name="password">
            <Input type="password" placeholder="New Password" />
          </Form.Item>

          <Form.Item name="password_confirmation">
            <Input type="password" placeholder="New Password Confirmarion" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Change my password
            </Button>
          </Form.Item>

      </Form>
    </Spin>
  );
};

export default PasswordsUpdate;
