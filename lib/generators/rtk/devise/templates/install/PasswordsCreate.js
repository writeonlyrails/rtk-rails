import React, {useEffect} from "react";
import { Form, Input, Button, Spin, Typography, message } from "antd";
import { useForm } from "antd/lib/form/Form";
import { usePasswordsCreateMutation  } from "../../services/users";

const PasswordsCreate = () => {
  const [passwordsCreate, { data, error, isLoading, isSuccess, isError }] = usePasswordsCreateMutation();
  const [form] = useForm();

  const onFinish = (values) => {
    passwordsCreate({ user: values })
    form.resetFields();
  };

  useEffect( () => {
    if (isError) {
      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }

    if (isSuccess) {
      console.log(data);
    }
  }, [data, error] )

  return (
    <Spin spinning={isLoading}>
      <Form form={form} onFinish={onFinish} name="passwords_create">
        <Typography.Title>Forgot your password?</Typography.Title>

        <Form.Item name="email">
          <Input type="email" placeholder="Email" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Send me reset password instructions
          </Button>
        </Form.Item>

      </Form>
    </Spin>
  );
};

export default PasswordsCreate;
