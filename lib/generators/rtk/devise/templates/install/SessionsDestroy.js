import React, { useEffect } from "react";
import { Form, Button, Spin, Typography, message } from "antd";
import { useForm } from "antd/lib/form/Form";
import { useSessionsDestroyMutation, useSessionsShowQuery } from "../../services/users";

const SessionsDestroy = () => {
  const { error: currentUserError, isLoading: currentUserIsLoading, isError: currentUserIsError, } = useSessionsShowQuery();
  const [ sessionsDestroy, { data, error, isLoading, isSuccess, isError } ] = useSessionsDestroyMutation(); 
  const [form] = useForm();

  const onFinish = () => {
    sessionsDestroy();
    form.resetFields();
  };

  useEffect(() => {
    if (currentUserIsError) {
      message.error(currentUserError.data.error);
    }
  }, [currentUserError]);

  useEffect(() => {
    if (isError) {
      message.error(error.data.error);

      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }

    if (isSuccess) {
      // Redirect
      // console.log(data);
    }
  }, [data, error]);

  return (
    <Spin spinning={isLoading || currentUserIsLoading}>
      <Form form={form} onFinish={onFinish} name="sessions_destroy">
        <Typography.Title>Log Out</Typography.Title>

        <Form.Item>
          <Button disabled={currentUserIsError} danger type="primary" htmlType="submit">
            Log Out
          </Button>
        </Form.Item>
      </Form>
    </Spin>
  );
};

export default SessionsDestroy;
