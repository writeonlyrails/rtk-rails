import React, { useEffect } from "react";
import { Spin, Typography, message } from "antd";
import { useSessionsShowQuery } from "../../services/users";

const SessionsShow = () => {
  const {
    data: currentUserData,
    error: currentUserError,
    isLoading: currentUserIsLoading,
    isError: currentUserIsError,
  } = useSessionsShowQuery();

  useEffect(() => {
    if (currentUserIsError) {
      for (const property in error.data.errors) {
        message.error(`${property} ${error.data.errors[property]}`);
      }
    }
  }, [currentUserError, currentUserError]);

  return (
    <Spin spinning={currentUserIsLoading}>
      <Typography.Title>Current User</Typography.Title>
      <p>{currentUserData && currentUserData.email}</p>
    </Spin>
  );
};

export default SessionsShow;
