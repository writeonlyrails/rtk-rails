# frozen_string_literal: true

module Rtk
  module Generators
    # Generates a scaffold for a new resource with Rails, React and Redux
    class ScaffoldGenerator < Rails::Generators::NamedBase
      class_option :apiName, required: true, desc: '', banner: ''

      def generate_resources
        generate "scaffold #{name} #{args.join(' ')} --skip-routes"

        data = "resources :#{name.pluralize.underscore}"
        scope = /scope 'api' do\s*\n/m

        insert_into_file 'config/routes.rb', optimize_indentation(data, 4), after: scope
      end

      def generate_query_endpoints
        generate "rtk:query:endpoints #{name} --apiName #{options[:apiName]}"
      end

      def generate_react_components
        generate "rtk:react:scaffold #{name} #{args.join(' ')} --apiName #{options[:apiName]}"
      end
    end
  end
end
