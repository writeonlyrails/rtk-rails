# frozen_string_literal: true

require 'rtk'

module Rtk
  module Redux
    module Generators
      # Sets up Redux
      class InstallGenerator < Rails::Generators::Base
        include Rtk::Helpers

        source_root File.expand_path('templates/install/', __dir__)

        def install_redux
          run 'yarn add @reduxjs/toolkit react-redux'
        end

        def setup_redux
          copy_file 'store.js.erb', 'app/javascript/app/store.js'

          insert_into_file 'app/javascript/packs/index.js',
                           "\nimport { Provider } from \"react-redux\"",
                           after: 'import { BrowserRouter } from "react-router-dom";'

          insert_into_file 'app/javascript/packs/index.js',
                           "import store from \"../app/store\"\n",
                           before: 'import App from "../App'

          replace_template_into_file 'app/javascript/packs/index.js',
                                     'App.js.erb',
                                     %r{[[:blank:]]<App />[[:blank:]]*\n}
        end
      end
    end
  end
end
