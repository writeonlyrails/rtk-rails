# frozen_string_literal: true

require 'rtk'

module Rtk
  module React
    module Generators
      # Sets up Webpacker and React
      class ScaffoldGenerator < Rails::Generators::NamedBase
        include Rtk::Helpers

        source_root File.expand_path('templates/scaffold/', __dir__)

        class_option :apiName,
                     required: true,
                     desc: '',
                     banner: ''

        def generate_index_component
          template 'Index.js.erb', "app/javascript/components/#{name.pluralize.camelize}Index.js"
        end

        def generate_show_component
          template 'Show.js.erb', "app/javascript/components/#{name.pluralize.camelize}Show.js"
        end

        def generate_create_component
          template 'Create.js.erb', "app/javascript/components/#{name.pluralize.camelize}Create.js"
        end

        def generate_update_component
          template 'Update.js.erb', "app/javascript/components/#{name.pluralize.camelize}Update.js"
        end

        def generate_destroy_component
          template 'Destroy.js.erb', "app/javascript/components/#{name.pluralize.camelize}Destroy.js"
        end
      end
    end
  end
end
