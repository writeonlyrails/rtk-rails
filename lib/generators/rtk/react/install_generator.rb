# frozen_string_literal: true

require 'rtk'

module Rtk
  module React
    module Generators
      # Sets up Webpacker and React
      class InstallGenerator < Rails::Generators::Base
        include Rtk::Helpers
        source_root File.expand_path('templates/install/', __dir__)

        def assert_requirements
          unless Rails.configuration.api_only
            raise Rails::Generators::Error, 'Error: Rtk::React is intended to be used with API only applications.'
          end
        end

        def setup_esbuild
          gem 'jsbundling-rails'
          run './bin/bundle install'
          run './bin/rails javascript:install:esbuild'
        end

        def setup_react
          run 'yarn add react react-dom'
          run 'yarn add react-router-dom@6'
          copy_file 'index.js.erb', 'app/javascript/packs/index.js'
          copy_file 'App.js.erb', 'app/javascript/App.js'
          remove_file 'app/javascript/packs/hello_react.jsx'
        end

        def setup
          copy_file 'static_pages_controller.rb.erb', 'app/controllers/static_pages_controller.rb'
          copy_file 'index.html.erb', 'app/views/static_pages/index.html.erb'
          insert_template_into_routes 'routes.rb.erb'
        end
      end
    end
  end
end
