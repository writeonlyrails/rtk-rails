# frozen_string_literal: true

require 'rtk/version'
require 'rtk/railtie'

module Rtk
  # Convenience functions to use in the generators
  module Helpers
    def insert_template_into_file(file, template, options)
      raw_template = File.join(self.class.source_root, template)

      insert_into_file file, options do
        ERB.new(IO.read(raw_template), trim_mode: '-').result(binding)
      end
    end

    def replace_template_into_file(file, template, flag)
      raw_template = File.join(self.class.source_root, template)

      gsub_file file, flag do
        ERB.new(IO.read(raw_template), trim_mode: '-').result(binding)
      end
    end

    def insert_template_into_routes(template)
      raw_template = File.join(self.class.source_root, template)
      route ERB.new(IO.read(raw_template), trim_mode: '-').result(binding)
    end
  end
end
