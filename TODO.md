# Devise

           Prefix        Verb   URI Pattern                                     Controller#Action
        new_user_session GET    /users/sign_in(.:format)                        devise/sessions#new                View. Needs to be handled by React.
            user_session POST   /users/sign_in(.:format)                        devise/sessions#create
    destroy_user_session DELETE /users/sign_out(.:format)                       devise/sessions#destroy
       new_user_password GET    /users/password/new(.:format)                   devise/passwords#new               View. Needs to be handled by React.
      edit_user_password GET    /users/password/edit(.:format)                  devise/passwords#edit              View. Needs to be handled by React.
           user_password PATCH  /users/password(.:format)                       devise/passwords#update
                         PUT    /users/password(.:format)                       devise/passwords#update
                         POST   /users/password(.:format)                       devise/passwords#create
cancel_user_registration GET    /users/cancel(.:format)                         devise/registrations#cancel
   new_user_registration GET    /users/sign_up(.:format)                        devise/registrations#new           View.
  edit_user_registration GET    /users/edit(.:format)                           devise/registrations#edit          View.
       user_registration PATCH  /users(.:format)                                devise/registrations#update
                         PUT    /users(.:format)                                devise/registrations#update
                         DELETE /users(.:format)                                devise/registrations#destroy
                         POST   /users(.:format)                                devise/registrations#create
   new_user_confirmation GET    /users/confirmation/new(.:format)               devise/confirmations#new           View.
       user_confirmation GET    /users/confirmation(.:format)                   devise/confirmations#show          View.
                         POST   /users/confirmation(.:format)                   devise/confirmations#create
         new_user_unlock GET    /users/unlock/new(.:format)                     devise/unlocks#new                 View.
             user_unlock GET    /users/unlock(.:format)                         devise/unlocks#show                View.
                         POST   /users/unlock(.:format)                         devise/unlocks#create

## React Components

- UsersCreate
- UsersUpdate
- UsersDestroy

- UsersSessionsCreate
- UsersSessionsDestroy

- PasswordsNew
  - URI: /users/password/new(.:format)

- PasswordsEdit
  - URI: /users/password/edit(.:format)

- ConfirmationsNew
  - URI: /users/confirmation/new(.:format)

- ConfirmationsShow
  - URI: /users/confirmation(.:format)

- UnlocksNew
  - URI: /users/unlock/new(.:format)

- UnlocksShow
  - URI: /users/unlock(.:format)

