require_relative 'lib/rtk/version'

Gem::Specification.new do |spec|
  spec.name        = 'rtk-rails'
  spec.version     = RTK::VERSION
  spec.authors     = ['Iago Bozza']
  spec.email       = ['iagobozza@writeonlycode.io']
  spec.homepage    = 'https://gitlab.com/writeonlyrails/rtk-rails/'
  spec.summary     = 'RTK Rails is a set of generators for Rails, React and Redux with Redux Toolkit and RTK Query.'
  spec.description = 'RTK Rails is a set of generators to help you set up and'\
    'manage your Rails/React/Redux application. It includes generators to install'\
    'and setup React and Redux with Redux Toolkit and RTK Query.'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # spec.metadata['allowed_push_host'] = 'TODO: Set to 'http://mygemserver.com''

  spec.metadata['homepage_uri'] = 'https://gitlab.com/writeonlyrails/rtk-rails'
  spec.metadata['source_code_uri'] = 'https://gitlab.com/writeonlyrails/rtk-rails'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/writeonlyrails/rtk-rails/-/blob/master/CHANGELOG.md'

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'rails', '~> 7.0.2', '>= 7.0.2'
end
